from django.urls import path

from . import views
from .views import MovieCreateView, CommentCreateView, RateCreateView

urlpatterns = [
    # path('', views.index, name='index'),
    path('', views.movies_list, name='movie-list'),
    path('<int:movie_id>/', views.movies_detail, name='movie-detail'),
    # path('<int:movie_id>/post_comment/', views.movies_comment, name='movie-post-comment'),
    # path('<int:movie_id>/post_rating/', views.movies_rating, name='movie-post-rating'),
    path('<int:movie_id>/post_comment/', CommentCreateView.as_view(), name='movie-post-comment'),
    path('<int:movie_id>/post_rating/', RateCreateView.as_view(), name='movie-post-rating'),
    path('new/', MovieCreateView.as_view(), name='movie-create')
]
